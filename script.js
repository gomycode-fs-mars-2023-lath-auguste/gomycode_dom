/******************************************** Declaration************************************************ */
                /****** Les constantes */  
const prixUnitaire1 = 8;
const prixUnitaire2 = 13;
const prixUnitaire3 = 17;

                /*** les variables */
// Les boutons souhaits
let heart_1 = document.getElementById('heart1');
let heart_2 = document.getElementById('heart2');
let heart_3 = document.getElementById('heart3');

// Facture
let prixHt = document.getElementById('prix-ht')
let prixTtc = document.getElementById('prix-ttc')
let shipping = document.getElementById('shipping')


/******************************************** Functions************************************************ */
window.onload = () =>{
    // initialisation au chargement du document
    let price_1 = document.getElementById('price1');
    let price_2 = document.getElementById('price2');
    let price_3 = document.getElementById('price3');

    price_1.value = prixUnitaire1;
    price_2.value = prixUnitaire2;
    price_3.value = prixUnitaire3;

    prixHt.value = parseFloat(price_1.value) + parseFloat(price_2.value) + parseFloat(price_3.value);
    shipping.value = prixHt.value * 0.01;
    prixTtc.value = parseFloat(prixHt.value) + parseFloat(shipping.value);
}

// Add au souhait
heart_1.onclick = () =>{
    alert('Vous avez ajouté ceci à vos souhaits !');
    heart_1.classList.remove("btn-danger") ;
    heart_1.classList.add("btn-success") ;
}
heart_2.onclick = () =>{
    alert('Vous avez ajouté ceci à vos souhaits !');
    heart_2.classList.remove("btn-danger") ;
    heart_2.classList.add("btn-success") ;
}
heart_3.onclick = () =>{
    alert('Vous avez ajouté ceci à vos souhaits !');
    heart_3.classList.remove("btn-danger") ;
    heart_3.classList.add("btn-success") ;
}


document.onclick = () =>{
    
    let price_1 = document.getElementById('price1');
    let price_2 = document.getElementById('price2');
    let price_3 = document.getElementById('price3');

    let nb1 = document.getElementById('nbre-article1');
    let nb2 = document.getElementById('nbre-article2');
    let nb3 = document.getElementById('nbre-article3');

    let listeDesPrix = []; // liste sur laquelle on va iterer pour recuperer les prix


    // Test de la presence de l'article dans le panier puis ajout de son prix à liste
    if(price_1){
        price_1.value = prixUnitaire1 * nb1.value;
        listeDesPrix.push(price_1.value)
    }
    
    if(price_2){
        price_2.value = prixUnitaire2 * nb2.value;
        listeDesPrix.push(price_2.value)
    }
    
    if(price_3){
        price_3.value = prixUnitaire3 * nb3.value;
        listeDesPrix.push(price_3.value)
    }
    
    let pricing = 0; // variable pour cumul des prix . notons qu on aurait pu utiliser .reduce()

    for(value of listeDesPrix){
        prixHt.value = 0;
        pricing += parseFloat(value);
    }
    
    prixHt.value = pricing; // nouveau prix apres iteration
    
    shipping.value = prixHt.value * 0.01;
    prixTtc.value = parseFloat(prixHt.value) + parseFloat(shipping.value);
}
/******************************************** Functions ************************************************ */

